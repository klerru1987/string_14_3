﻿#include <iostream>
#include <string>

using namespace std;

int main()
{
    setlocale(LC_ALL, "RU");

    string core = "Intel";
    cout << "Название ядра " << core << endl
         << "Длинна строки " << core.size() << endl
         << "Первый символ в строке " << core[0] << endl
         << "Последний символ в строке " << core[(core.size()-1)] << endl;
    return 0;
}